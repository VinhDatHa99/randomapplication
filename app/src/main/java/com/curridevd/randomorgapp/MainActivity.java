package com.curridevd.randomorgapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView txtNumber;
    EditText edtNumber1, edtNumber2;
    Button  btnGenerate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AnhXa();

        btnGenerate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {

                String stringInMin = edtNumber1.getText().toString().trim();
                String stringInMax = edtNumber2.getText().toString().trim();

                //ep kieu du lieu tu chuoi sang so
                //if(stringInMax.length() == 0 || stringInMin.length()==0)
                if(stringInMin.isEmpty() || stringInMax.isEmpty())
                {
                    Toast.makeText(MainActivity.this, "Please input enough number", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    int min = Integer.parseInt(stringInMin);
                    int max = Integer.parseInt(stringInMax);

                    if(min > max)
                    {
                        Toast.makeText(MainActivity.this, "Max number must greater than min number", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Random random = new Random();
                        int result = random.nextInt(max - min + 1) + min;
                        txtNumber.setText(result + "");
                    }
                }
            }
        });
    }

    private void AnhXa()
    {
        txtNumber = (TextView) findViewById(R.id.textViewNumber);
        edtNumber1 = (EditText) findViewById(R.id.editTextNumberMin);
        edtNumber2 = (EditText) findViewById(R.id.editTextNumberMax);
        btnGenerate = (Button) findViewById(R.id.buttonGenerate);
    }
}